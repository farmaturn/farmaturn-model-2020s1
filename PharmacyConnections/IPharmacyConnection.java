

import java.net.ConnectException;

import connections.Configuration;

public interface IPharmacyConnection {
    void connect() throws ConnectException;

    String getName();

    void getMessagesStream() throws ConnectException;

    Configuration getConfiguration();

    void setConfiguration(Configuration config);
}
