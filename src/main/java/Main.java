import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import common.utils.Constants;
import connections.FarmaturnClassLoader;
import connections.FarmaturnConnectionsRegister;
import connections.IClassFinder;

public class Main {

	private static IClassFinder classLoader = new FarmaturnClassLoader();
	private static FarmaturnConnectionsRegister connectionRegister = new FarmaturnConnectionsRegister(classLoader);
	private static List<String> paths = new ArrayList<String>(Arrays.asList(Constants.PATH_CONNECTIONS));

	public static void main(String[] args) {
		initApplication();
	}

	public static void initApplication() {
		try {
			connectionRegister.discoverConnections(paths);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
