package common;

import java.lang.reflect.Field;

// ESTA CLASE VA A CONVERTIR CUALQUIER BODY QUE RECIBA EN EL FORMATO CORRECTO DE ENVIO
public class ExternalProxyIC implements IExternalIC {
	private IExternalIC sm;

	public ExternalProxyIC() {
		super();
	}

	public ExternalProxyIC(IExternalIC proxyImplemt) {
		super();
		this.sm = proxyImplemt;
	}

	@Override
	public Object send(Object header, Object data) {
		// TODO: REALIZARLE UN FORMATEO CORRECTO.
		// TODO: UTILIZAR MAS SERVICIOS PARA MAPEAR LOS CAMPOS SEGUN LOS TIPOS
		// SOPORTADOS POR EL SISTEMA
		
		StringBuilder serviceStringed = new StringBuilder();
		StringBuilder stringedFieldItem = new StringBuilder();
		for (Field field : data.getClass().getDeclaredFields()) {
			stringedFieldItem = new StringBuilder();
			stringedFieldItem.append("CLASE ENVIADA <" + field.getDeclaringClass() + "> \n");
			stringedFieldItem.append("ATRIBUTO <" + field.getName() + "> \n");
			stringedFieldItem.append("CLASE <" + field.getType() + "> \n");
			try {
				stringedFieldItem.append("VALOR <" + field.get(data) + "> \n");
				
			} catch (Exception e) {
				stringedFieldItem.append("<NO PUDE CONVERTIR EL VALOR: " + e.getMessage() + " > \n");
			}
			finally {
				serviceStringed.append(stringedFieldItem.toString());
			}
		}
		return sm.send(header, serviceStringed.toString());
	}

	@Override
	public void setConnection(IExternalIC ic) {
		this.sm = ic;
	}
	
}
