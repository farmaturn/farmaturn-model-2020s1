package common;

// esta clase es la generica para todo lo que sean llamados externos al sistema
public interface IExternalIC {
	// TODO: AGREGAR EL RESTO DE METODOS COMO "CONSUME", ETC.
	public Object send(Object header, Object data);
	
	public void setConnection(IExternalIC ic);
}
