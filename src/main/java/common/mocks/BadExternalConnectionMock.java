package common.mocks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BadExternalConnectionMock {
	// CLASE MOCK PARA PROBAR UNA CONECCION CORRECTA
	public String otro_text;
	public Date otro_fecha;
	public byte otro_algo;
	public List<String> otro_textos;
	
	public BadExternalConnectionMock() {
		this.otro_text = "BAD CONNECTION CASE";
		this.otro_algo = Byte.MIN_VALUE;
		this.otro_fecha = new Date();
		List<String> textosAux = new ArrayList<>();
		textosAux.add("PEPE");
		textosAux.add("SAMANTA");
		textosAux.add("SANTIAGO");
		this.otro_textos = textosAux;
	}
}
