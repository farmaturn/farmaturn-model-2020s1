package common.mocks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GoodExternalConnectionMock {
	// CLASE MOCK PARA PROBAR UNA CONECCION CORRECTA
	public String text;
	public Date fecha;
	public byte algo;
	public List<String> textos;
	
	public GoodExternalConnectionMock() {
		this.text = "GOOD CONNECTION CASE";
		this.algo = Byte.MAX_VALUE;
		this.fecha = new Date();
		List<String> textosAux = new ArrayList<>();
		textosAux.add("PEPE");
		textosAux.add("SAMANTA");
		textosAux.add("SANTIAGO");
		this.textos = textosAux;
	}
}
