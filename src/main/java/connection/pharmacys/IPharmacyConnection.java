package connection.pharmacys;

import java.net.ConnectException;

import connections.Configuration;

public interface IPharmacyConnection {
    void connect() throws ConnectException;

    String getName();

    Configuration getConfiguration();

    void setConfiguration(Configuration config);
}
