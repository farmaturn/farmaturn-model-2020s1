package connection.pharmacys.concrete;

import java.net.ConnectException;

import connection.pharmacys.IPharmacyConnection;
import connections.Configuration;


/**
 * Concrete implementation for TKL Pharmacy
 * @author Dario Rick
 */
public class PharmacyTkl implements IPharmacyConnection {

	private Configuration _config;
	
	public PharmacyTkl() {
		_config = new Configuration();
		_config.setParameter("IP", "127.0.0.1");
		_config.setParameter("Port", "443");
		_config.setParameter("User", "admin");
		_config.setParameter("Password", "tkl123");
	}
	
	@Override
	public void connect() throws ConnectException {
		// TODO Auto-generated method stub
	}

	@Override
	public String getName() {
		return "Farmacias TKL San Miguel";
	}

	@Override
	public Configuration getConfiguration() {
		return _config;
	}

	@Override
	public void setConfiguration(Configuration config) {
		_config = config;
	}

}
