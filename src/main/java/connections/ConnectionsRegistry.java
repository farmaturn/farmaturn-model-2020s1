package connections;

import java.util.HashMap;
import java.util.Map;

import connection.pharmacys.IPharmacyConnection;

public class ConnectionsRegistry {
    private static Map<String, IPharmacyConnection> INSTANCE;

    public static Map<String, IPharmacyConnection> getInstance() {

        if (INSTANCE != null) {
            return INSTANCE;
        } else {
            INSTANCE = new HashMap<>();
            return INSTANCE;
        }
    }
}
