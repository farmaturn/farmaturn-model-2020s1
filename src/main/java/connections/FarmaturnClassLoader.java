package connections;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

public class FarmaturnClassLoader extends ClassLoader implements IClassFinder {

	@Override
	public List<Class> findClasses(String path) {
		List<Class> classes = new ArrayList<Class>();

		try {
			final URL url = getResource(path);
			if (url != null) {
				try {
					final File apps = new File(url.toURI());

					List<URL> classUrls = new ArrayList<URL>();
					for (File app : apps.listFiles()) {
						classUrls.add(app.toURL());
					}

					URL[] arrayUrl = new URL[classUrls.size()];
					for (int i = 0; i < classUrls.size(); i++) {
						arrayUrl[i] = classUrls.get(i);
					}

					URLClassLoader cl = URLClassLoader.newInstance(arrayUrl);
					for (URL classUrl : classUrls) {
						String concreteClassName = new File(classUrl.getPath()).getName().replace(".class", "");
						String fullPath = path.replace('/', '.').concat(".").concat(concreteClassName);
						Class c = cl.loadClass(fullPath);
						classes.add(c);
					}

				} catch (URISyntaxException ex) {
					ex.printStackTrace();
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return classes;
	}
}
