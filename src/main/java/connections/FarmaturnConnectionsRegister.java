package connections;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import common.utils.Constants;
import connection.pharmacys.IPharmacyConnection;

public class FarmaturnConnectionsRegister {

	private IClassFinder _finder;

	public FarmaturnConnectionsRegister(IClassFinder finder) {
		_finder = finder;
	}

	public void discoverConnections(List<String> paths) throws Exception {
		for(String path : paths) {
			List<Class> clases = _finder.findClasses(path);
			List<IPharmacyConnection> connections = new ArrayList<>();
			Constructor cons;

			for (Class clazz : clases) {
				if (IPharmacyConnection.class.isAssignableFrom(clazz)) {
					cons = clazz.getConstructor();
					IPharmacyConnection s = (IPharmacyConnection) cons.newInstance();
					connections.add(s);
				}
			}

			for (IPharmacyConnection s : connections) {
				ConnectionsRegistry.getInstance().put(s.getName(), s);
			}
		}
	}

}