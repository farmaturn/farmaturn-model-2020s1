package connections;

import java.util.List;

public interface IClassFinder {

	List<Class> findClasses(String path);
}
