package entities;

public interface IHasCode {
	String getCode();
}
