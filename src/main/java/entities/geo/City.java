/**
 * 
 */
package entities.geo;

import entities.IHasCode;

/**
 * @author Dario Rick
 *
 */
public class City implements IHasCode{
	private String code;
	private String cityName;
	private State state;
	
	
	@Override
	public String getCode() {
		return code;
	}
}
