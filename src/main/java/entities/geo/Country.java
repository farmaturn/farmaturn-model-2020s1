/**
 * 
 */
package entities.geo;

import entities.IHasCode;

/**
 * @author Dario Rick
 *
 */
public class Country implements IHasCode {
	private String code;
	private String countryName;
	
	
	@Override
	public String getCode() {
		return code;
	}
}
