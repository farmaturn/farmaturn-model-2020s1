/**
 * 
 */
package entities.geo;

import entities.IHasCode;

/**
 * @author Dario Rick
 *
 */
public class State implements IHasCode {
	private String code;	
	private String stateName;
	private Country country;
	
	
	@Override
	public String getCode() {
		return code;
	}
}
