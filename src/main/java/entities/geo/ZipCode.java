/**
 * 
 */
package entities.geo;

import entities.IHasCode;

/**
 * @author Dario Rick
 *
 */
public class ZipCode implements IHasCode {
	private String code;
	private State state;
	private double latitude;
	private double longitude;
	private Iterable<City> cities;

	@Override
	public String getCode() {
		return code;
	}
}
