package common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import common.mocks.BadExternalConnectionMock;
import common.mocks.GoodExternalConnectionMock;
import connection.pharmacys.concrete.PharmacyTkl;

/**
 * @author Dario Rick
 *
 */
public class ExternalProxyICTest {
	
	/**
	 * @author Dario Rick
	 */
	@Test 
	public void GoodConnectionTest() {
		try {
			ExternalProxyIC externalProxy = new ExternalProxyIC(new ExternalProxyIC());
			
			GoodExternalConnectionMock mock = new GoodExternalConnectionMock();
			Object response = externalProxy.send("Header", mock);
			
			assertNotNull(response);
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
	/**
	 * @author Dario Rick
	 */
	@Test 
	public void BadConnectionTest() {
		try {
			ExternalProxyIC externalProxy = new ExternalProxyIC(new ExternalProxyIC());

			
			BadExternalConnectionMock mock = new BadExternalConnectionMock();
			Object response = externalProxy.send("Header", mock);
			
			assertNotNull(response);
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}
	
}
