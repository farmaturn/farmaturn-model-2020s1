package connections;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import common.utils.Constants;
import connection.pharmacys.IPharmacyConnection;
import connection.pharmacys.concrete.PharmacyTkl;

/**
 * @author Dario Rick
 *
 */
public class FarmaturnConnectionsRegisterTest  {

	private static IClassFinder classLoader = new FarmaturnClassLoader();
	private static FarmaturnConnectionsRegister connectionRegister = new FarmaturnConnectionsRegister(classLoader);
	private static List<String> paths = new ArrayList<String>(Arrays.asList(Constants.PATH_CONNECTIONS));

	/**
	 * @author Dario Rick Asserts true if a valid concrete IPharmacyConnection is
	 *         loaded
	 */
	@Test
	public void ValidConnectionTest() {
		try {
			connectionRegister.discoverConnections(paths);
			IPharmacyConnection connection = ConnectionsRegistry.getInstance().get("Farmacias TKL San Miguel");
			assertEquals(connection.getClass(), new PharmacyTkl().getClass());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Dario Rick Asserts true if a class which not implements the
	 *         IPharmacyConnection is requested, and the classloader returns null
	 */
	@Test
	public void InvalidConnectionTest() {
		try {
			connectionRegister.discoverConnections(paths);
			IPharmacyConnection connection = ConnectionsRegistry.getInstance().get("TestInvalidConnection");
			assertEquals(connection, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Dario Rick Asserts true if an unexistant connection is requested, and
	 *         the classloaded returns null
	 */
	@Test
	public void NotFoundConnectionTest() {
		try {
			connectionRegister.discoverConnections(paths);
			IPharmacyConnection connection = ConnectionsRegistry.getInstance().get("UnexistantConnection");
			assertEquals(connection, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
