package connections.pharmacys.concrete;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import connection.pharmacys.IPharmacyConnection;
import connection.pharmacys.concrete.PharmacyTkl;

/**
 * @author Dario Rick
 *
 */
public class PharmacyTklTest {	
	
	@Test 
	public void DummyTest() {
		IPharmacyConnection tklConnection = new PharmacyTkl();
		assertNotEquals(tklConnection, null);
		
		tklConnection.setConfiguration(new connections.Configuration());
		assertNotNull(tklConnection.getConfiguration());
	}
	
}
