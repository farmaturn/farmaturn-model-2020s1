package connections.pharmacys.concrete;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import connection.pharmacys.concrete.TestInvalidConnection;

/**
 * @author Dario Rick
 *
 */
public class TestInvalidConnectionTest {
	
	
	@Test 
	public void DummyTest() {
		TestInvalidConnection invalidConnection = new TestInvalidConnection();
		assertNotEquals(invalidConnection, null);
	}
	
}
